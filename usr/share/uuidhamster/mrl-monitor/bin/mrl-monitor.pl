#!/usr/bin/perl

use strict; use warnings;

BEGIN {
    unshift @INC, "/usr/share/uuidhamster/mrl-monitor/lib";
    unshift @INC, "/usr/share/uuidhamster/mojolicious/lib";
}

my $VERSION = '{$project}.{$version}';

use Mojolicious::Lite;

use Util::APIKeyGenerator;

my $default_config = {
    hypnotoad       => {
        listen          => [ 'http://127.0.0.1:1336' ],
        workers         => 1,
        pid_file        => '/var/run/uuidhamster/mrl-monitor.pid',
    },
    log             => {
        path            => '/var/log/uuidhamster/mrl-monitor.log',
        level           => 'debug',
    },
    api_keys        => [
        Util::APIKeyGenerator::generate()
    ],
    cors_origins    => [
        'http://localhost',
        'https://localhost',
    ],
    secrets         => [],
};

# configure application
plugin Config => {
    file    => '/etc/uuidhamster/mrl-monitor.conf',
    default => $default_config,
};

# configure secrets
app->secrets((ref app->config->{secrets} eq "ARRAY") ? app->config->{secrets} : [ app->config->{secrets} ]);

# configure logging
app->log(Mojo::Log->new(
    path        => app->config->{log}->{path}   || undef,
    level       => app->config->{log}->{level}  || 'info',
));

# load plugins
plugin 'Plugin::RateLimitMonitor';

sub error {
    my ($self, $exception) = @_;
    $self->app->log->error($exception->message());

    $self->render(
        status  => $exception->status(),
        text    => $exception->message(),
        json    => { message => $exception->message() },
    );
}

hook before_routes => sub {
    my ($self) = shift;

    # check api key
    my $key         = $self->tx->req->headers->header('X-HamsterAPI-Key');
    my $valid_key   = 0;

    foreach (@{$self->config->{api_keys}}) {
        $valid_key = (defined $key && $key eq $_);
    }

#    return error($self, Exception::InvalidAPIKeyException->new($key)) unless $valid_key;

    # set response header for CORS requests that do not support preflight requests
    $self->res->headers->header('Access-Control-Allow-Origin' => join(",", @{$self->config->{cors_origins}}));
};

# answer to preflight requests (CORS)
options '*' => sub {
    my $self = shift;

    $self->res->headers->header('Access-Control-Allow-Credentials'  => 'true');
    $self->res->headers->header('Access-Control-Allow-Methods'      => 'OPTIONS, GET, POST, DELETE, PUT');
    $self->res->headers->header('Access-Control-Allow-Headers'      => 'Accept', 'Content-Type', 'X-CSRF-Token', 'X-HamsterAPI-Key');
    $self->res->headers->header('Access-Control-Max-Age'            => '1728000');

    $self->respond_to(any => { data => '', status => 200 });
};

get '/status' => sub {
    my ($self) = @_;
    $self->render(
        json    => $self->ratelimit->status(),
    );
};

post '/add' => sub {
    my ($self) = @_;

    $self->ratelimit->add();
    $self->render(
        json    => { message => "ok" },
    );
};

app->start;
