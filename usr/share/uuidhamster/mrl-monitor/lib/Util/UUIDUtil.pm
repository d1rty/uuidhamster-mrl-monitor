package Util::UUIDUtil;

sub regexp {
    return qr/^(?:uuid:)?([[:xdigit:]]{8})-?([[:xdigit:]]{4})-?([[:xdigit:]]{4})-?([[:xdigit:]]{4})-?([[:xdigit:]]{12})$/;
}

sub to_UUID {
    return (shift =~ regexp())
        ? sprintf("%s-%s-%s-%s-%s", $1, $2, $3, $4, $5)
        : undef;
}

sub to_lazy_UUID {
    return (shift =~ regexp())
        ? sprintf("%s%s%s%s%s", $1, $2, $3, $4, $5)
        : undef;
}

sub is_UUID {
    return (shift =~ regexp());
}

1;
