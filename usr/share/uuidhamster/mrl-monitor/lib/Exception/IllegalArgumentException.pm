package Exception::IllegalArgumentException;
use base 'Exception::BasicException';

sub new {
    my ($class) = shift;
    return $class->SUPER::new((defined $_) ? sprintf('Argument %s not unterstood', @_) : 'argument expected');
}

sub code {
    return 400;
}

1;
