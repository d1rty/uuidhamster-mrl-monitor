package Exception::BasicException;

sub new {
    my ($class, $message) = @_;

    return bless {
        class   => $class,
        message => $message,
    }, $class;
}

sub code {
    return 500;
}

sub class {
    return shift->{class};
}

sub message {
    return shift->{message};
}

1;
