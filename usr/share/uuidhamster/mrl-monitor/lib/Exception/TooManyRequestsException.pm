package Exception::TooManyRequestsException;
use base 'Exception::BasicException';

sub new {
    return shift->SUPER::new('Rate limit exceeded. Please try again later');
}

sub code {
    return 429;
}

1;
