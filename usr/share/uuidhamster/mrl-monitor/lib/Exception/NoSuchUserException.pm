package Exception::NoSuchUserException;
use base 'Exception::BasicException';

sub new {
    return shift->SUPER::new(sprintf 'Username %s not found', shift);
}

sub code {
    return 404;
}

1;
