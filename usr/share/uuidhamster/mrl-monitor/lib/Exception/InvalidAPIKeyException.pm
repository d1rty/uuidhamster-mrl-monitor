package Exception::InvalidAPIKeyException;
use base 'Exception::BasicException';

sub new {
    my ($class, $key) = @_;
    return $class->SUPER::new((defined $key) ? sprintf('Unknown API key: %s', $key) : 'No API key provided');
}

sub code {
    return 401;
}

1;
