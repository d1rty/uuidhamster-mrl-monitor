package Exception::NoSuchUUIDException;
use base 'Exception::BasicException';

sub new {
    return shift->SUPER::new(sprintf 'UUID %s not found', shift);
}

sub code {
    return 404;
}

1;
