package Exception::CachePolicyException;
use base 'Exception::BasicException';

sub new {
    my ($class, $policy, $message) = @_;
    return $class->SUPER::new(sprintf('policy "%s" in effect: %s', $policy, $message));
}

sub code {
    return 204;
}

1;
