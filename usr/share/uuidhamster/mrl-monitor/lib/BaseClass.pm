package BaseClass;


=head1 NAME

BaseClass


=head1 SYNOPSIS

use BaseClass;



=head1 DESCRIPTION

This package may be used for any package where you want a unified error handling mechanism. In addition you can safely create an instance of an package using the C<create()> method. When doing so, errors while instancing this object are catched and can be read out of the error mechanism.

All you need to do for these features is to tell a package that it's base package is C<BaseClass> like so:

  package MyPackage;

  use BaseClass;
  use base qw(BaseClass);

Note that this package registers the following object hashkeys if C<$self>:

=over 2

=item _error_msg

The error message itself

=back

Note that you must not overwrite these keys.


=head1 DEPENDENCIES 

This package has no dependencies.


=head1 ERROR HANDLING

To register an error in your package you may call these methods:

=over 4

=item C<register_error()> / C<register_critical_error()>

This methods saves an error message (the first parameter to this method). Besides the C<register_critical_error()> method sets the _critical_error to true (1).

=item C<has_error()>

This method returns true (1) or false (0), depending on if $self->{_error_msg} is defined or not.

=item C<get_error()>

Return the error message.

=back

=cut

use strict; use warnings;


=head1 METHODS

=over 4

=item C<new>

This is a generic constructor that Creates a new instance of a package. This constructor takes a single arg and saves it to $self->{args}.

=back

=cut
sub new {
    my ($class) = shift;
    my ($args) = @_;

    # enabling debug feature
    my $debug = 0;
    $debug = delete $args->{DEBUG};
    $debug = 1 if ($ENV{PERL_DEBUG});

    my $self = bless {
        args    => $args,
        DEBUG   => $debug,
    }, $class;

    $self->reset_error();

    if ($self->can("configure")) {
        $self->configure($args);
    }

    return $self;
}


=over 4

=item C<create>

Safely creates an instance of an package. When doing so, errors while instancing this object are catched and can be read out of the error mechanism.

=back

=cut
sub create {
    my ($class) = shift;
    my ($args) = @_;

    # create a new instance. This instance will be overwritten if $class could be instanciated
    my $self = bless {}, $class;
   
    eval "require $class";
    if ($@) {
        $self = __PACKAGE__->new();
        $self->register_error($@);
    } else {
        $self = $class->new($args);
    }
    return $self;
}


=over 4

=item C<register_error>

Registers an error message.

=back

=cut 
sub register_error {
    my ($self) = shift;
    my $caller = (caller)[0];

    ($self->{_error_msg})  = @_;
    $self->debug("Error: ".$self->{_error_msg}, $caller);
    return 0;
}


=over 4

=item C<reset_error>

Resets all errors.

=back

=cut
sub reset_error {
    my ($self) = shift;
    my $caller = (caller)[0];
    $self->{_error_msg}         = undef;
    $self->{_critical_error}    = 0;
#    $self->debug("Resetting errors", $caller);
}


=over 4

=item C<has_error>

Returns boolean if the instance of the package had an error or not.

=back

=cut
sub has_error {
    return 0 unless defined shift->{_error_msg};
    return 1; 
}


=over 4

=item C<get_error>

Returns the error message

=back

=cut
sub get_error {
    return shift->{_error_msg} || undef;
}

sub debug {
    my ($self, $message) = @_;
#    $main::log->debug(sprintf("[%s(%s)] %s", (caller)[0], (caller)[2], $message));
}


=head1 AUTHOR

Tristan Cebulla <tristan.cebulla@1und1.de>, 1&1 Internet AG


=head1 BUGS

There are no known bugs.


=head1 COPYRIGHT & LICENSE

Copyright (c) 2011 Tristan Cebulla (<tristan.cebulla@1und1.de>). All rights reserved.

This module is free software; you can redistribute it and/or modify it under the same terms as
Perl itself. See L<perlartistic>.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

=cut

1;
