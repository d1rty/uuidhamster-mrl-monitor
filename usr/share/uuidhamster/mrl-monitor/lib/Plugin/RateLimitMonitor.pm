package Plugin::RateLimitMonitor;
use Mojo::Base 'Mojolicious::Plugin';

use strict; use warnings;

my $query_limit_time    = 600;
my $query_limit         = 600;
my $monitor             = [];

sub _get_size {
    my ($self) = @_;
    return scalar @$monitor || 0;
}

sub _is_below_query_limit {
    my ($self) = @_;
    return ($self->_get_size() < $query_limit);
}

sub _is_below_time_limit {
    my ($self) = @_;
    return ($self->_get_wait_time() < $query_limit_time);
}

sub _get_wait_time {
    my ($self) = @_;

    my $now         = time();
    my $oldest      = ($self->_get_size() > 0) ? $monitor->[0] : $now - $query_limit;
    my $time_diff   = $now - $oldest;
    my $wait_time   = ($time_diff < $query_limit) ? 0 : $time_diff - $query_limit;

    return $wait_time;
}

sub _get_min_age {
    my ($self) = @_;
    return ($self->_get_size() > 0) ? $monitor->[0] : 0;
}

sub _cleanup {
    my ($self) = @_;

    foreach (@$monitor) {
        if ($_ < time() - $query_limit_time) {
            splice(@$monitor, 0, 1)
        }
    }

    return 1;
}

sub _status {
    my ($self) = @_;
    return {
        query_limit_ok  => $self->_is_below_query_limit(),
        time_limit_ok   => $self->_is_below_time_limit(),
        min_age         => $self->_get_min_age(),
        wait_time       => $self->_get_wait_time(),
        size            => $self->_get_size(),
    };
}

sub _add {
    my ($self) = @_;
    push @$monitor, time();
}

sub register {
    my ($self, $app) = @_;

    $app->helper('ratelimit.add' => sub {
        my ($c, @args) = @_;
        $app->log->debug('Adding event to rate limit monitor');
        return $self->_add();
    });

    $app->helper('ratelimit.status' => sub {
        my ($c, @args) = @_;

        $self->_cleanup();
        my $status  = $self->_status();

        $c->app->log->debug(sprintf('Rate limit status: query limit %s - time limit %s (min age: %d - wait time: %d seconds - size: %d)',
            ($status->{query_limit_ok}) ? 'ok' : 'not ok',
            ($status->{time_limit_ok})  ? 'ok' : 'not ok',
            $status->{min_age},
            $status->{wait_time},
            $status->{size},
        ));

        return $status;
    });
}

1;
